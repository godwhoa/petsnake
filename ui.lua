require 'grid'
require 'pet'
require 'libs.shiny'
require 'classic'

UI = Object:extend()

function UI:new()
	self.pet =  Pet()
	self.classic = Classic()
	self.gamemode = "menu"
	self.oldmode = "menu"
	self.menu = Shiny(
	{255,238,228},
	{197,233,155},
	{0,0,0},
	'assets/font.ttf',
	40,50
	)
	self.menu:addlabel('center',100,"Snake Games")
	self.menu:addbutton('center', 200, 190, 50, 'Classic', function()
		self.gamemode = "classic"
	end)
	self.menu:addbutton('center', 250, 190, 50, 'Pet Snake', function()
		self.gamemode = "pet"
	end)
end
function UI:mpressed(x, y, button)
	if self.gamemode == "menu" then
		self.menu:mousepressed(x, y, button)
	end
end

function UI:key(key)
	if key == "q" and self.gamemode ~= "menu" then
		self[self.gamemode]:reset()
		self.gamemode = "menu"
	end
	if self[self.gamemode].key ~= nil then
		self[self.gamemode]:key(key)
	end
end

function UI:update(dt)
	if self.gamemode ~= self.oldmode then
		love.window.setTitle(self.gamemode)
		self.oldmode = self.gamemode
	end
	if self[self.gamemode].update ~= nil then
		self[self.gamemode]:update(dt)
	end
end

function UI:draw()
	self[self.gamemode]:draw()
	if self.gamemode ~= "menu" then Grid:draw() end
end