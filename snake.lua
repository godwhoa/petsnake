Snake = Object:extend()

function Snake:new(len)
	-- for easy access
	self.x,self.y=0,0
	-- timestep
	self.step = 1/10
	self.time = 0
	-- direction
	self.dir = ""
	-- segments of snake {{x,y},...}
	self.seg = {{x=math.floor(love.math.random(ww/sw)),y=math.floor(love.math.random(wh/sh))}}
	-- initial len.
	self:add(len)
end

function Snake:add(len)
	for i=1,len do
		local last = {x=self.seg[#self.seg].x,y=self.seg[#self.seg].y}
		-- it trickles down so we'll just clone the last segment
		table.insert(self.seg,last)
	end
end

function Snake:movement()
	-- trickle down the movement head to tail.
	for i=#self.seg,2,-1 do
		self.seg[i].x = self.seg[i-1].x
		self.seg[i].y = self.seg[i-1].y
	end
	--teleport if out of bounds
	if self.seg[1].x < 0 then
		self.seg[1].x = ww/sw
	end
	if self.seg[1].y < 0 then
		self.seg[1].y = wh/sh
	end
	if self.seg[1].x > math.floor(ww/sw) then
		print(math.floor(ww/sw))
		self.seg[1].x = 0
	end
	if self.seg[1].y > math.floor(wh/sh) then
		self.seg[1].y = 0
	end
	--direction
	if self.dir == "up" then
		self.seg[1].y = self.seg[1].y - 1
	end
	if self.dir == "down" then
		self.seg[1].y = self.seg[1].y + 1
	end
	if self.dir == "left" then
		self.seg[1].x = self.seg[1].x - 1
	end
	if self.dir == "right" then
		self.seg[1].x = self.seg[1].x + 1
	end
	self.x,self.y = self.seg[1].x,self.seg[1].y
end

function Snake:update(dt)
	self.time = self.time + dt
	if self.time >= self.step then
		self.time = self.time - self.step
		self:movement()
	end
	scolor[2] = (255*dt)
end

function Snake:draw()
	for i,s in ipairs(self.seg) do
		scolor[4] = (255/i)
		love.graphics.setColor(scolor)
		if i == 1 then
			love.graphics.rectangle("fill", s.x*sw, s.y*sh, sw, sh)
			love.graphics.setColor(lcolor)
			love.graphics.rectangle("fill", s.x*sw, s.y*sh, sw/2, sh/2)
			love.graphics.rectangle("fill", s.x*sw+sw/2, s.y*sh+sh/2, sw/2, sh/2)
		else
			love.graphics.rectangle("fill", s.x*sw, s.y*sh, sw, sh)
		end
	end
end