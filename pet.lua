require 'snake'

Pet = Object:extend()

function Pet:new()
	-- self.ping = true
	self.snake = Snake(4)
end

function Pet:update(dt)
	local mx, my = love.mouse.getPosition()
	print(mx/sw,my/sh)
	mx,my = math.floor(mx/sw),math.floor(my/sh)
	-- print(string.format("snakex: %d snakey: %d mx: %d my: %d\n",self.snake.x,self.snake.y,mx,my))
	if mx < self.snake.x then
		self.snake.dir = "left"
	end 
	if mx > self.snake.x then
		self.snake.dir = "right"
	end
	if my < self.snake.y then
		self.snake.dir = "up"
	end 
	if my > self.snake.y then
		self.snake.dir = "down"
	end 
	self.snake:update(dt)
end
function Pet:key(key)
	if key == "w" then
		self.snake:add(1)
	else
		self.snake.dir = key
	end
end

function Pet:reset()
	self.snake.seg = {{x=love.math.random(ww/sw),y=love.math.random(wh/sh)}}
end
function Pet:draw()
	self.snake:draw()
end