Object = require 'libs.classic'
require 'libs.math'
require 'def'
require 'grid'
require 'ui'

love.window.setMode(ww,wh,{msaa=8})
love.window.setTitle(wtitle)

function love.load()
	love.graphics.setBackgroundColor(bakcolor)
	Grid:load()
	ui = UI()
end
function love.mousepressed(x, y, button)
	ui:mpressed(x, y, button)
end

function love.keypressed(key)
	ui:key(key)
end

function love.update(dt)
	ui:update(dt)
end

function love.draw()
	ui:draw()
end
