tfx = require "termfx"
require 'snake'

Classic = Snake:extend()

function Classic:new()
	Classic.super.new(self,1)
	self:reset()
	tfx.init()
end

function fancy(s)
	local x = math.floor((tfx.width() - #s) / 2)
	local y = math.floor(tfx.height() / 2)
	for i=1, #s do
	    tfx.setcell(x, y, string.sub(s, i, i))
	    x = x + 1
	    tfx.present()
	end
end


function Classic:check()
	if self.x == self.fruit.x and self.y == self.fruit.y then
		self:add(1)
		self.fruit = {x=math.floor(love.math.random((ww/2)/sw)),y=math.floor(love.math.random((wh/2)/sh))}
		self.score = self.score + 1
	end
	--head coll.
	if #self.seg > 2 then
		for i=2,#self.seg do
			if self.seg[i].x == self.x and self.seg[i].y == self.y then
				self:reset()
				break
			end
		end
	end
end

function Classic:update(dt)
	self.time = self.time + dt
	if self.time >= self.step then
		self.time = self.time - self.step
		self:movement()
		self:check()
		tfx.clear(tfx.color.WHITE, tfx.color.BLACK)
		fancy(string.format("x: %d y: %d dir: %s",self.x,self.y,self.dir))
	end
end

function Classic:draw()
	love.graphics.setColor(0,0,0)
	if self.score ~= self.oldscore then
		love.window.setTitle(("Score: %d"):format(self.score))
		self.oldscore = self.score
	end
	love.graphics.rectangle("fill", self.fruit.x*sw, self.fruit.y*sw, sw, sh)
	for i,s in ipairs(self.seg) do
		scolor[4] = (255/i)
		love.graphics.setColor(scolor)
		if i == 1 then
			love.graphics.rectangle("fill", s.x*sw, s.y*sh, sw, sh)
			love.graphics.setColor(lcolor)
			love.graphics.rectangle("fill", s.x*sw, s.y*sh, sw/2, sh/2)
			love.graphics.rectangle("fill", s.x*sw+sw/2, s.y*sh+sh/2, sw/2, sh/2)
		else
			love.graphics.rectangle("fill", s.x*sw, s.y*sh, sw, sh)
		end
	end
end
function Classic:key(key)
	if key == "up" and self.dir ~= "down" then
		self.dir = "up"
	elseif key == "left" and self.dir ~= "right" then
		self.dir = "left"
	elseif key == "w" then
		self:add(1)
	elseif key == "right" and self.dir ~= "left" then
		self.dir = "right"
	elseif key == "down" and self.dir ~= "up" then
		self.dir = "down"
	end
end

function Classic:randfruit()
	self.fruit = {x=math.floor(love.math.random(ww/sw/2)),y=math.floor(love.math.random(wh/sh/2))}
end

function Classic:randsnake()
	self.seg = {{x=math.floor(love.math.random(ww/sw/2)),y=math.floor(love.math.random(wh/sh/2))}}
end

function Classic:reset()
	self.score = 0
	self.oldscore = 0
	self:randfruit()
	self:randsnake()
end